---
layout: markdown_page
title: "Recruiting Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Metrics

The recruiting team pulls metrics reports each month, quarter, and year, as well as routinely for the People Operations [Group Conversation](https://about.gitlab.com/handbook/people-operations/group-conversations/).

The monthly metrics reports can be found in the [Google Drive](https://drive.google.com/drive/u/1/folders/1UNisqJAJQbYiEplNKG0FsgKQkx4-qoHA) and is only accessible to those who contribute to and review the reports, as they contain confidential information about team members and candidates. The reports contain several tabs: "Summary", "Recruiting", "eNPS", "Diversity", "Low Location Factor", and "Turnover". The recruiting team is responsible for the Recruiting, eNPS, and Diversity tabs, as well as summarizing their findings on the Summary tab. The reports are done in the month following the month that we are analyzing so that we are able to have the full picture (e.g. January's metrics report is done in February).

#### Apply to Offer Accept (Days)

The number of days between a candidate applying to a vacancy and accepting an offer in a given month. The Apply to Offer Accept (Days) target is < 30.

#### Candidate NPS

The answer to the ‘Candidate Net Promoter Score’ (NPS) question, “Overall, my interviewing experience was a positive one,” is greater than 4.1 across all departments in a given month (1 - 5 response scale).

#### Candidates Sourced vs. Candidates Hired 

How many candidates were sourced out of those who we hired in a given month. The candidates sourced versus candidates hired target is greater than 0.28.

#### Diversity Lifecycle: Applications, Recruited, Interviewed, Offers Extended, Offers Accepted, and Retention

The recruiting workflow metrics specific to candidates who self-identified as being diverse.

#### Hires vs. Plan

The number of hires based on start date divided by the number of planned hires in a given month. The Hire vs. Plan target is >0.9.

The number of hires are measured in BambooHR located in the “hire date” field and the number of planned hires is located in Google Sheets. 

#### New Hire Location Factor

The [average location factor](/handbook/people-operations/people-operations-metrics/#average-location-factor) of all newly hired team members in a given period (default period is one calendar month).

#### Offer Acceptance Rate

The number of offers accepted divided by the number of offers extended in a given month. The offer acceptance rate target is > 0.9.

The offer acceptance rate is measured in Greenhouse.

### Recruiting Tab of Monthly Metrics Report

#### Structure

Currently, the Recruiting tab consists of a metric "scorecard" that reports on the overall metrics as well as broken out into divisions. The scorecard includes:

- Number of hires
- Number of offers
- Offer acceptance rate
- Number of (known) diverse hires
- Percentage of (known) diverse hires
- Average days from Apply to Accept (aka days to hire)
- Candidate satisfaction
- Number of hires from low location index
- Percent of hires from low location index
- Number of hires from outside of the US
- Percent of hires from outside of the US
- Number of roles sourced
- Number of referrals made
- Number of referrals hired

The Recruiting tab continues to include information regarding the hiring plan vs the hiring progress of each month vs overall, the number of openings as of the end of the month as well as the average number of openings per recruiter, and a candidate funnel of the past three months to evaluate volume of applicants and pass through rate of each stage. Finally, a full list of hires for that month is created, which includes:

- Candidate name
- Days from Apply to Accept
- Applied date
- Offer date
- Hired date (aka the date the candidate signs their contract)
- Division
- Vacancy name
- Source
- Location (city, state (if applicable), and country)
- Location factor (high or low)

#### How to pull the report

To get the data for the recruiting tab, you will need to go to the [Greenhouse Reports page](https://app2.greenhouse.io/reports) and pull a variety of reports.

The first report you should pull is the "[Hiring Speed per Candidate](https://app2.greenhouse.io/reports/hiring_speed)" report. Click "Filters and more" and change "Open" jobs to "All" jobs, save, then click "Include Migrated Candidates", and click "Apply". Then download the report by clicking the down arrow "Export to Excel" and imported the downloaded report to a Google Sheets document. (It's best to keep a working spreadsheet for all reports needed in a month, which you would then pick and pull the important data out of and into the official monthly metrics spreadsheet.) This report will give you a full list of all hires, so you will need to delete all hires except for those hired in the month you are evaluating. To do so, change the "Format" of the "Accepted Date" column to "Date", then filter by oldest to newest, and delete all rows before and after the month in question. Copy over the name of the candidates, their vacancies, sources, applied date, offer date, and accepted date into the monthly metrics report. You will want to create your own formula for Days from Apply to Accept, as Greenhouse analyzes the Days to Offer, which is the day we move a candidate to the offer stage but we use the day a candidate signs their contract; to do this, use the `datedif` formula, using the "Applied on" and "Accepted date" dates.

The next report you will pull is the "[Offer Activity](https://app2.greenhouse.io/reports/offer_summary)". Click "Filters and more" and change "Open" jobs to "All" jobs, save, click "Include Migrated Candidates", and choose activity date of "Previous Month"; then click apply and download the report and import it into your working spreadsheet. The report shows how many candidates moved to offer stage and for some reason were rejected in the previous month. You can sort the report by "Offers Rejected" to see which jobs had a candidate that moved to offer but rejected. You will then need to go into the job dashboard for each of these jobs and filter to find the candidate(s) in question. Make sure the rejection reason is accurate (we are only looking to collect data on candidates who declined our offers), and if there is not a specific reason why a candidate declined our offer reach out to the recruiter for more information. Include your finds in the monthly metrics report scorecard and be sure to break it out by division.

Next, you will want to pull a report of the diversity data for the hired candidates for the month. You will pull an [EEOC](https://app2.greenhouse.io/reports/eeoc) report (only able to be pulled by Site Admins with permission to do so), change the filter to include "All" jobs, and download/import the **expanded** report. Next, do a `vlookup` to tie the hired candidate's names to the list of candidates in the EEOC report. Do not copy over the specifics of each candidate, but count how many diverse hires we had and in what divisions.

The next report is the "[Pipeline History and Pass-through Rates](https://app2.greenhouse.io/reports/interviewing_pipeline)" report, which you will use to create the applicant funnel.

Next, you will pull a "[Referrals Over Time](https://app2.greenhouse.io/reports/referrals)" report, and change the filters to "all jobs" and have the columns be "Month", not "Week". Then you will copy over the number of referrals overall for the month you are evaluating, as well as for each division.

Finally, you will pull an "[All Jobs Summary](https://app2.greenhouse.io/reports/jobs_summary)" report, with the filters of "All" jobs and including migrated candidates, download/import it into Google Sheets, and calculate the `sum` of all openings for "Open" jobs. Copy that number into the number of openings field in the monthly metrics report, and then divide it by the number of recruiters to get the average number of vacancies per recruiter.

### eNPS Tab of Monthly Metrics Report

#### Structure

The eNPS report consists of two eNPS evaluations, candidate interview NPS and onboarding eNPS. Recruiting is responsible for the candidate interview eNPS evaluation.

The candidate interview NPS is collected through a candidate survey sent out from Greenhouse one week after a candidate is either rejected or hired, and it is only sent out to candidates who advanced to the team interview stage or later. The recruiting team evaluates the number of responses made in the month in question, and then provides the average score of those responses. Then the responses are broken out by division, so we can evaluate which divisions are providing the best candidate experiences and where we can improve.

#### How to Pull the Report

To get the data for the candidate interview NPS, you will need to go to the [Greenhouse Reports page](https://app2.greenhouse.io/reports), and select the premade report "[Candidate Surveys](https://app2.greenhouse.io/reports/candidate_surveys)". Next, click on "Filters and more", choose a date range of "Previous Month", then click "Apply". Then download the report by clicking the down arrow "Export to Excel" and importing the downloaded report to a Google Sheets document. We have a [pre-existing Google Sheets](https://docs.google.com/spreadsheets/d/1vOVqcOCu3vJNMXMwvKWBhRUUDYx5s50zIOMnghK-FJU/edit#gid=1779752772) that contains all of our Candidate Survey results since we joined Greenhouse, so you will need to import the report as a new sheet.

Once you have the report in Google Sheets, put a filter on the data, and sort Row D "1. Overall, my interviewing experience was a positive one." Add a new column to the left of Row D and title the new column "Score". Correlate each of the following responses with the appropriate number:

- Strongly Agree = 5
- Agree = 4
- Neutral = 3
- Disagree = 2
- Strongly Disagree = 1

You will then need to create another new column next to the "Departments" column and title it "Division". Based on the department listed for each response, enter in the correlating division.

Once all divisions are added, create a pivot table so you can view how many of each score each division got. Then you will input this data into the master monthly metrics spreadsheet.

### Diversity Tab of Monthly Metrics Report

#### Structure

The Diversity report is split out into evaluation of current team members and all candidates. The recruiting team is responsible for the candidates portion, and we pull information on the provided EEOC data that applicants can choose to submit during their application. We evaluate gender and race/ethnicity at this time.
