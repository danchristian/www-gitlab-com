---
layout: markdown_page
title: "UX Department"
---

### On this page

{:.no_toc}

- TOC
{:toc}

# UX Department

We make a software product that is easy to use, enables everyone to contribute, and is built for a diverse, global community. To achieve this vision, the UX department actively works with the wider GitLab community to understand user behaviors, needs, and motivations. And as a skilled team of UX practitioners, we go far beyond the basics of UI design (that's only a part of what we do), proactively helping to shape the product experience through generative user research, strategic UX initiatives, and useful technical documentation.  

To meet our goals, the UX Department works cross functionally with internal teams and community contributors. We believe in iterative, valuable, and proactive improvements. When we get things wrong, we quickly strive to make them right. We're passionate about the GitLab product, and we strive to become subject-matter experts both in our specific stage groups and across the whole product.

## UX Vision

Our principles are that GitLab should be **[productive](#productive), [minimal](#minimal), and [human](#human)**. We want to [*design*](https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434) the most complete, uncomplicated, and adaptable DevOps tool on the market, enabling our users to spend more time adding business value in their own jobs.

#### Productive
*Yielding favorable or useful results; constructive: a productive suggestion.*

**User Experience is part of everything we do.**
* The default assumption is that user experience is part of every issue.

**Everyone at the company understands the role of UX and everyone contributes to the user experience.**
* Encourage Product Managers, Engineers, and the wider GitLab community to contribute wireframes, low-fidelity designs, and prototypes. [Everyone is a designer](https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434).
* Designers involve Product Managers, Engineers, and the wider community in problem discovery and definition.
* Designers, Product Managers, and Engineers are involved in UX research, backing solutions with data.

**Our solutions make users feel confident and efficient.**
* Use the simplest most boring solution for a problem.
* Understand the user's goals and circumstances.
* Respect the importance of their work, and avoid gimicky details.
* Provide sufficient feedback and direction to achieve user goals.
* Design holistic experiences and workflows.

#### Minimal
*Simplicity driving maximum effect.*

**Design is about iteration, so we continuously iterate on our processes, too.**
* Aggressively break down issues into the smallest effort that gets results.
* The design system will never be finished; it should always be evolving.
* Whenever possible, start in low-fidelity (wireframes, greyscale, etc.) to make sure we get it right before we go any further.

**Minimize distractions and clutter so users can focus.**
* Understand the user journey and goals.
* Create hierarchy and a sense of direction to guide users through the flow.
* Maintain a strong information architecture.
* Don't be afraid to remove things that don't contribute to the user flow.

#### Human
*Self-aware. Emotional, yet rational. Understandable and helpful.*

**Be Ambitious**
* Think big, but break it down into small steps.
* Don’t be afraid to fail. Permission to fail builds a culture of experimentation.
* Quirkiness is part of our DNA; we should embrace it in the right moments and contexts.

**Be Helpful**
* See the world through other people's eyes, and try to understand their experiences deeply and meaningfully.
* Don’t overload the user.
* Don’t change established user flows, if not really necessary.
* Assist the community in making an impact on our product.
* Steer the user in the right direction if they end up in a “bad” place (without blaming them), and recognize their efforts and accomplishments!

## How we work

The UX Department works alongside the community, Product Managers (PMs), Frontend engineers (FE), and Backend engineers (BE). PMs are responsible for kicking-off initiatives, taking action, and setting the direction of the product. PMs don't own the product; they gather feedback and give GitLab team members and the wider community space to suggest and create.

UX should assist in driving the [product vision](/direction/product-vision/) early in the process. We inform the vision by conducting generative research, as well as facilitating discussion with community members, customers, PM, FE, and BE. We are **elevated** above just the transactional workflow and **generative** in creating work rather than just executing tasks.

### Proactive and reactive UX
We'll always have responsibility for reactive tasks that help "keep the lights on," such as bug fixes and minor UX enhancements. But we also must carve out some portion of our time for proactive work that identifies existing pain points, redefines how we approach problems, and uncovers actionable innovation and improvement opportunities. This proactive UX work enables us to create the most complete, competitive, and resilient solutions. 

It isn’t always easy to find the space for proactive UX work, but the challenge is worth it, because it's how we create a best-in-class product experience. And it's not just the UX team's responsibility; it requires a coordinated effort from Leadership, Product, and Engineering, too.

We're currently working to find the right balance between proactive and reactive UX work. While we don't have a prescriptive ratio, we also don't allot 100% of our available work time to reactive efforts. Before and at the start of each milestone, UXers should work with their managers to define the appropriate ratio, based on our active OKRs and stage group requirements. When there are questions about priority or scope, or when a UXer is concerned about meeting a deadline, they should immediately reach out to their manager to help them resolve any concerns. Comunicate early and often!

#### Expectations
Every UXer: 
* Make sure discipline peers are aware of proactive UX work and what we need from them to succeed.
* Let your manager know right away, if you believe you're not trending to complete your work. The sooner your manager knows, the higher the likelihood they can resolve the issue and manage expectations.
* Account for time off (both yours and others) as best you can.
* Work efficiently. Lean UX and design reuse allow us to do more with less. Ask yourself, "What's the least we can do to solve and implement this issue *well*?" Start lo-fi, and only increase fidelity as design confidence grows and implementation requires. Leverage the design system, and cross-share learnings and assets.

Managers:
* Set a baseline capacity for proactive/reactive work each milestone; for example, 30%/70% respectively. Teams can use this information to balance work and manage expectations. 
* Quantify strategy work with clear time-to-complete (TTC) expectations, measurable goals, and deadlines.
* Resolve team questions, concerns, and blockers quickly.
* Make sure cross-functional partners are aware of UX OKRs and their associated dependencies.

#### Examples of proactive UX
One example of proactive UX work is our [Experience Baselines and Recommendations](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) initiative. Another example is the generative research that our UX Researchers lead (while inviting cross-functional partners to participate). Yet another example is the ongoing effort to beautify our UI, both through [small, tactical changes](https://gitlab.com/groups/gitlab-org/-/epics/989) and through our [Pajamas Design System][pajamas].

**Experience Baselines and Recommendations** <br>
Designers use [Experience Baselines](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) to benchmark common user tasks. In many cases, tasks involve multiple stages of the product, giving designers visibility into how users traverse across stages. Designers follow with [Experience Recommendations](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) for how to improve the experience in upcoming milestones.

### Holistic UX

Though we structure our work around individual stages of the product (Plan, Manage, Create, etc.), we can't separate look, feel and process from what a user is trying to accomplish. We must maintain a focus on what the user needs to get done and how to deliver that in the most effective manner possible, including how users flow from one stage of the product to another. Maintaining a holistic overview of the path a user may take allows us to see the possible twists and turns along the way. With this knowledge, we can optimize the user experience.

It is the responsibility of each [Product Designer](https://about.gitlab.com/job-families/engineering/product-designer/) and [UX Researcher](https://about.gitlab.com/job-families/engineering/ux-researcher/) to understand how users may flow in and out of their assigned stage group(s). This means that everyone must understand the broader product, how stage groups relate, and the work that is planned and in process outside of their own focus area(s).

### Easy UX

GitLab solves complex problems, but that doesn't mean our product experience needs to be complicated. We always strive to design solutions that are easy to use, but without oversimplifying the product. 

We acknowledge we're building a product that must support skilled power users, while still making it [easy for less experienced users](https://imposter-syndrome.lol/posts/a-few-thoughts-on-ember/#the-issues-with-learning-ember) and [non-developers](https://about.gitlab.com/handbook/product/#enabling-collaboration) to learn. Within each stage group, the learning curve must be at least comparable to our best-in-class competitors, with clear, cohesive workflows, a highly usable interface, and comprehensive documentation.  

### Stable counterparts

Every Product Designer is aligned with a PM and is responsible for the same features their PM oversees.
Technical Writers each support multiple stage groups and at least one complete stage (up to four) with the goal of supporting only one stage per writer by the end of FY-2020 per the current hiring plan. UX Researchers support multiple PMs from across one or more stages. 

UXers work alongside PMs and engineering at each stage of the process: planning, discovery, implementation, and further iteration. The area a Product Designer or Technical Writer is responsible for is part of their title; for example, "Product Designer, Plan." You can see which area of the product each Product Designer or Technical Writer is aligned with in the [team org chart](/company/team/org-chart/).

Product Designers and Technical Writers may also serve as a "backup" for other areas of the product. This area will be listed on the [team page](/company/team/) under their title as an expertise; for example, "Plan expert." UX backups should be just that&mdash;backups. They are there to conduct UX reviews on MRs when the assigned UXer for that area is out. The UX lead for a given area should coordinate with the PM and their backup during scheduling for any work that is critical. Critical UX work is defined as any work that addresses an outage, a broken feature with no workaround, or the current workaround is unacceptable.

#### Headcount planning

In the spirit of having "stable counterparts," we plan headcount as follows:

* **One Product Designer for every stage group**
    * 1:1 ratio of Product Designers to Product Managers
    * Approximately a 1:7 ratio of Product Designers to Engineers
* **One Technical Writer for up to four stage groups**
    * 1:3 ratio of Technical Writers to stage groups
    * Approximately a 1:21 ratio of Technical Writers to Engineers
* **One UX Researcher for every stage**
    * Approximately a 1:5 ratio of UX Researchers to Product Managers
    * Approximately a 1:35 ratio of UX Researchers to Engineers

#### Everyone can contribute

The UX department is not solely responsible for the user experience of GitLab. Everyone is encouraged to contribute their thoughts  on how we can make GitLab better by opening an issue. You can use just words or include images. These images can take a variety of forms; here are just a few examples:

* Drawings or sketches to convey your idea
* Wireframes made using a software of your choosing (Balsamiq, Sketch, etc.)
* High-fidelity mockups made using the [Pajamas Design System][pajamas]
* Screenshots of changes to our UI, made by manipulating the DOM in the browser

If you are creating high-fidelity designs, please make sure to let others know that this is a proposal and needs UX review. You can ping anyone on the UX team for assistance.

### Iteration

Here at GitLab, iteration means making the [smallest thing possible and getting it out as quickly as possible](/handbook/values/#iteration). Working like this allows us to reduce cycle time and get feedback from users faster, so we can continue to improve quickly and efficiently.

Iteration isn't just one of GitLab’s six founding values, [C.R.E.D.I.T](/handbook/values/#credit), it is one of the [foundational concepts in design thinking and user experience](https://www.interaction-design.org/literature/article/design-iteration-brings-powerful-results-so-do-it-again-designer). Planning too far ahead without real-world feedback can cause you to build something that doesn't meet user needs.

Iteration is especially vital in an open-source community. Keeping changes small and iterative makes it easy for anyone to contribute. Here are some examples of how we are embracing the power of iteration and using it to build GitLab:

* We aggressively break issues down into the smallest scope possible. If an effort is too big to be completed in one milestone, then it is too big. [Epics](/handbook/product/#how-to-use-epics) allow us to maintain a holistic view of an area, while breaking the work down into an MVC.
* We keep a list of improvement issues that are actively seeking contributions from the community. They are small in scope, allowing the community to contribute designs or code to the issues. You can learn more about it in the [Community Contributions](/handbook/engineering/ux/ux-department-workflow/#community-contributions) section of this handbook. You can also [view the list of issues that need UX work](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=Accepting+merge+requests&label_name[]=UX).
* You may notice that our [Design System](https://gitlab.com/gitlab-org/design.gitlab.com) has a lot of "to-do" items. Rather than try to tackle everything at once, we are gradually populating our component library and incrementally rolling those components out to production.

### Design culture
The culture of the design department is characterized by the following:
* We're independent, autonomous, and not hierarchical.
* We have a high level of commitment and reliability to our team.
* We have ownership over our work and are motivated to step up, take risks, and go beyond our roles.
* We don't ask for permission, and we believe that everyone can be a part of the design process.
* We are passionate, kind, honest, direct, transparent, inclusive, and unafraid to ask for help.
* We work asynchronously, we value iteration, and our designs are never complete.
* We are open to ideas and actively collaborate with other teams.
* We are willing to use whatever tools or mediums best communicate our design solutions.

## UX Resources

### UX Workflow details

Read about [__UX Department workflows__](/handbook/engineering/ux/ux-department-workflow).

Read about [__Product Designer workflows__](/handbook/engineering/ux/ux-designer)

Read about [__UX Researcher workflows__](/handbook/engineering/ux/ux-research)


### GitLab design project

The [GitLab design project](https://gitlab.com/gitlab-org/gitlab-design/) is primarily used by the Product Design team to host design files and hand them off for implementation. For details, please visit the [README](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md) for the project.

### Pajamas Design System

The GitLab Design System, [Pajamas][pajamas], was developed to increase iteration speed and bring consistency to the UI through reusable and robust components. This system helps keep the application [DRY](http://programmer.97things.oreilly.com/wiki/index.php/Don't_Repeat_Yourself) and allows designers to focus their efforts on solving user needs, rather than recreating elements and reinventing solutions. It also empowers Product, Engineering, and the Community to use these defined patterns when proposing solutions. The Design System can be viewed at [design.gitlab.com](https://design.gitlab.com/). It is currently a work in progress.

The project and repository for design.gitlab.com can be found [here](https://gitlab.com/gitlab-org/design.gitlab.com).

### GitLab SVGs

Our [GitLab SVGs](https://gitlab.com/gitlab-org/gitlab-svgs) repository manages all SVG assets by creating an SVG sprite out of icons and optimizing SVG-based illustrations.

Take a look at our [SVG library](http://gitlab-org.gitlab.io/gitlab-svgs/).

### UX research project

The UX research project contains all research undertaken by GitLab's UX researchers. [This project](https://gitlab.com/gitlab-org/ux-research) is used for the organization and tracking of UX research issues only.

### UX design archive

The [UX design archive](/handbook/engineering/ux/design-archive) is a
collection of key design issues broken down by specific areas of GitLab. It is
not a comprehensive list. It is intended to shed insight into key UX design
decisions.

### System usability score
Once each quarter, we run a [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html) survey to measure user perception of the GitLab product. We send the survey to members of the wider GitLab community, with the goal of asking for a response from any individual no more than twice per year.

* [Q1 FY20 results](/handbook/engineering/ux/sus/System-Usability-Scale-Q1FY2020.pdf)

## UX on social media

It is encouraged to share UX designs and insight on social media platforms such as Twitter and Dribbble.

### Twitter

You can contribute design-related posts to our [@GitLab Twitter account](https://twitter.com/gitlab) by adding your tweet to our [UX Design Twitter spreadsheet][twitter-sheet].

1. Add a new row with your tweet message, a relevant link, and an optional photo.
1. Ensure that your tweet is no more than 280 characters. If you’re including a link, ensure you have enough characters, and consider using a [link shortening service](https://bitly.com/).
1. The UX Lead will check the spreadsheet at the beginning of each week and schedule any tweets on Tweetdeck.
1. Once a tweet is scheduled, the tweet will be moved to the "Scheduled" tab of the spreadsheet.

### Dribbble

GitLab has a [Dribbble team account](https://dribbble.com/GitLab) where you can add work in progress, coming soon, and recently released works.

* If a Dribbble post has a corresponding open issue, link to the issue so designers can contribute on GitLab.
* Add the Dribbble post to our [UX Design Twitter spreadsheet][twitter-sheet], along with a link to the corresponding open issue if applicable.
* If you’re not a member of the GitLab Dribbble team and would like to be, contact the UX Lead to grant you membership.


## About our team

This section is inspired by the recent trend of Engineering Manager READMEs. _e.g._ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe). Get to know more about the people on our team!

* [Christie Lenneville](/handbook/engineering/ux/one-pagers/christie-readme) - UX Director
* [Valerie Karnes](https://gitlab.com/vkarnes/readme) - UX Manager
* [Sarah O'Donnell](/handbook/engineering/ux/one-pagers/sarahod-readme) - UX Research Manager
* [Rayana Verissimo](https://gitlab.com/rverissimo/readme) - Sr. Product Designer
* [Matej Latin](https://gitlab.com/matejlatin/focus) - Sr. Product Designer
* [Jacki Bauer](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md) - UX Manager

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[everyone-designer]: https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434
[pajamas]: https://design.gitlab.com
