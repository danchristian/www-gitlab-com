---
layout: markdown_page
title: "Group Conversations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Over the years, GitLab has had Functional Group Updates, which are regular updates from a group at GitLab.
For examples please see recent dates in [a search for them](/blog/#stq=functional+group+updates&stp=1).

We are evolving FGUs to no longer be updates, but to be conversations, and will now be calling them "Group Conversations". The slides and trainings included below are still relevant but should be considered the starting point for a great conversation with other team members around the work that your group is doing. All team members at GitLab should feel comfortable and encouraged to join these conversations, not just as a listener to the content, but as a participant in the conversation.

## Engage

If you are unable to attend the Group Conversation, don't let that hold you back!  Watch the latest Group Conversation in the Google Drive folder found in the description of each update.  If you have questions or a discussion to start, bring it to the `#group-conversation` Slack channel! Make sure to @tag the presenter!

## Make it worthwhile

1. Save time and ensure asynchronous communication by writing information on the slides. Many people will not be able to participate in Group Conversations, either live or recorded, but can read through the slides.
1. Link a Google Doc from the calendar item and link your slides from the Doc. Use the Doc to line up questions that are then verbally asked in the call.
1. Do not present your slides. Start with call by stating your name and title and ask people for verbal questions and when relevant mention a slide number. This is similar to what we do during [board meetings](/handbook/board-meetings/).
1. If you want to present please consider [posting a recording to YouTube](/handbook/communication/youtube/) at least a day before the meeting, link it from the Google Doc, and mention it in the relevant slack channels. 
1. It is not the end of the world if a call ends early, we aim for results, not for spending the time allotted.
1. Don't do a countdown when you're asking for questions, people will not ask one. Just end the call or even better say: we'll not end the call before getting at least one question.
1. People need to be on time for the company call, end no later than 29 minutes after the hour and preferably at 25 minutes after the hour.
1. If there are more questions or a need for longer conversation, mention on what chat channel the conversation can continue or link to a relevant issue.

## Conversation

1. Please enter your questions or comments on the linked Google Doc. Preface your question with your full name (first and last name) because there might be other people with your first name on the call and it's helpful to newcomers if they are distinguished. However, keep in mind that Group Conversations are recorded and shared publicly and that it's okay to opt out of using your name due to safety and privacy concerns.
1. Ask your question as it comes up in the queue.
1. Do not ask questions in zoom chat.
1. Not everything has to be a question.
1. If you have a comment, bias to putting it into the Google Doc so that those who weren't able to attend the meeting live can see what you had to say (and any responses that arose from it).
1. Thanking and recognizing people is very important.
1. You can ask someone to present a slide to get more context.

## Format

1. Please see the above paragraph about making it worthwhile.
1. We'll use Zoom and all non-confidential group conversations will be live streamed to YouTube
1. Use this [slide deck](https://docs.google.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.
1. Make sure to add the link of the presentation to the GitLab Team Meetings Calendar invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation,  and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. To do this, go to the [GitLab Team Meetings calendar](/handbook/tools-and-tips/#gitlab-availability-calendar), find the event, click on `more details` and edit the description. People Ops will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Reduce distractions for yourself and the attendees by:
   - having the presentation open in its own new browser window, and only sharing that window in Zoom.
   - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).
1. All calls are published on YouTube, with the exception of Finance, Sales, Security, Channel, and Partnerships. Every call is reviewed prior to publishing to make sure no internal information is shared externally.
1. Right now everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Attendance is optional.
   - Please add the link to your Group Conversation recordings folder in Google Drive to the invite. This enables team members to easily review the recording afterward.
1. The conversation is also announced on and the recording linked from our company call agenda.
1. Tone should be informal, like explain to a friend what happened in the group last month, it shouldn't require a lot of presentation.
1. You can invite someone in the team to give the update, it doesn't need to be the team lead, but the team lead is responsible that it is given.
1. Calls are 5 times a week 30 minutes before the company call, 8:00 to 8:25am Pacific.
1. Calls are scheduled by an [EA](/job-families/people-ops/executive-assistant/).
1. If you need to reschedule, please *switch* your presenting day with another Group Conversation leader. If you've agreed to switch do the following:
    - Go to the *GitLab Team calendar* invite
    - Update the date of your and the other invite to be switched
    - Choose to send an update to the invitees
    - _If prompted_ with the questions to update 1 or all events, choose to only update this event
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos", which is accessible to all users with a GitLab.com e-mail account.
1. Video recordings will be published on our blog so contributors, users, and customers can see it. We're aiming to publish a blog post once a week of that weeks' recordings with the matching slides in one go.
1. People tend to spend at least an hour to prepare their update, you write down everything people should know and present only for a few minutes.
1. Slides with a lot of text that can be read on their own with lots of links are appreciated.
1. There are three layers of content in a presentation:
  - Data, this is the contents the slide.
  - Take away, this is the title of the slide, so use: 'migration 10 days ahead of schedule', instead of 'migration schedule estimates', the combined titles of your slides should make a good summary.
  - Feelings, this is the verbal and non-verbal communication in the video feed, how you feel about the take away, 'I'm proud of the band for picking up the pace'.

## Group Conversation Training

In this video our CEO, Sid gives our team tips and tricks for their FGU. This still applies to Group Conversations.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MN3mzvbgwuc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Suggestions

- Enable “Show document outline” under the View menu to navigate this document more easily
- Use numbered lists instead of bulleted lists.
- Have a few empty items for questions (space + enter).
- Do not use chat, it is very much OK to put encouragements and thanks in the doc.
- When asking questions, please use your full name as we have many people with the same first name.
- Ask people to verbalize their question (unless you know they aren't in the call), if they can't (not in call, driving, audio problems) read the questions for people watching only the video.
- Give a heads up in company announcements if there is a video for the group conversation

## Template for the blog post

For instructions on how to create a blog post, please see our [Blog Handbook](/handbook/marketing/blog/#create-a-new-post).

Please copy the code block below and paste it into a blog post with the file name `yyyy-mm-dd-group-conversation.html.md`.

```md
---
title: "GitLab's Functional Group Updates - MMM DD-DD" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: Functional Group Updates
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Company call](/handbook/#company-call), a different Group initiates a [conversation](/handbook/people-operations/group-conversations/) with our team.

The format of these calls is simple and short where attendees have access to the presentation content and presenters can either give a quick presentation or jump straight into the agenda and answering questions.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

```

## Livestream the Video

- In order to get access to the GitLab YouTube account, follow the instructions in the secure note in 1Password.
- To upload the video, go to YouTube and click the up arrow at the top right corner, next to the GitLab profile picture.
- Change the security level from "public" to either "unlisted" (only those with the link can view) or "private" (only people with access to the GitLab YouTube account can view), so that you can edit the video prior to it being live.
- Under "Basic Info", change the title to follow this pattern: "Group Conversation XXX-Team Date". Change the description to:

Group Conversation XXX-Team Date

Presentation - https://example.com [Input your presentation's URL and check if its set to view only for anyone with the link if using Google Slides]

Try GitLab EE FREE for 30 days, no credit card required - /free-trial/

Want to know more about our free GitLab.com? /gitlab-com/

GitLab License FAQ - /products/

Questions?? - /company/contact/


- After the video is done uploading, click "Video Manager" in the bottom right corner.
- Edit the video to start when the meeting actually starts:
  - Click **Edit** next to the video icon.
  - Click the **Enhancements** tab on the top menu bar.
  - Click **Trim** on the bottom right. Slide the left edge of the bar to a few moments before the presentation begins, and the right edge of the bar to a few moments after the presentation ends. Click **Done**.
- Take a screenshot of the second slide of the presentation to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the **Info and Settings** tab when you are editing a video.
- After the video is finished being edited, change the security level back to "public".
- Make sure to use correct format for [embedding videos from YouTube](/handbook/product/technical-writing/markdown-guide/#display-videos-from-youtube) into the blog post. Replace the video URL only.

## Schedule

There is a rotating schedule with each functional group having a conversation on a regular interval.  Please let one of the People Operations Specialists know if you need to reschedule at least one week in advance so we may be able to help you find a new slot. If you need to reschedule last minute, please find someone willing to switch spots with you, or ask someone on your team to take over the call.  If you do reschedule a call on your own, please let the specialists know so we can update the calendar! The schedule is roughly as follows: 

#### Week One
- Monday: [Secure Section](/handbook/product/categories/#secure-sub-department)
- Tuesday: [Ops Section](/handbook/product/categories/#ops-sub-department)
- Wednesday: [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
- Thursday: [Marketing](/handbook/marketing/)

#### Week Two
- Monday: [Security](/handbook/engineering/security/)
- Tuesday: [Dev Section](/handbook/product/categories/#dev-sub-department)
- Wednesday: [Quality](/engineering/quality/)
- Thursday: [UX](/engineering/ux/)

#### Week Three
- Monday: [Growth Section](/handbook/product/categories/#growth-sub-department)
- Tuesday: [Support](/handbook/support/)
- Wednesday: [Development](/handbook/engineering/development/)
- Thursday: [Finance](/handbook/finance/)

#### Week Four
- Monday: [Security](/handbook/engineering/security/)
- Tuesday: [General](/handbook/ceo/)
- Wednesday: [CRO Group Conversation](/handbook/sales/)
- Thursday: [People Operations](/handbook/people-operations/)

#### Week Five
- Monday: [Product](/handbook/product/)
- Tuesday: [CI/CD Section](/handbook/product/categories/#cicd-sub-department)
- Wednesday: [Product Marketing](/handbook/marketing/product-marketing/)
- Thursday: [Infrastructure](/handbook/engineering/infrastructure/)

#### Week Six
- Monday: [Security](/handbook/engineering/security/)
- Tuesday: [Meltano](/handbook/meltano/)
- Wednesday: [Alliances](/handbook/alliances/)
- Thursday: [Community Relations](/handbook/marketing/community-relations/)

## Livestreaming the Call

All group conversations will be livestreamed via Zoom's webinar feature.  To create a webinar, go to Zoom under the People Ops account and add a webinar.  To start the webinar, join and select the more options in zoom.  Choose livestream to youtube, and upload the video live.  Promote all participants to panelist to be able to contribute.

Note if the call will be a public or private live stream.  In the event that a call has something come up that needs to stay internal, make sure to change it to a private live stream.

The calendar events for all livestreamed Group Conversations should adhere to our YouTube [visibility guidelines](/handbook/communication/youtube/#visibility).

## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions and discussions live or in chat
