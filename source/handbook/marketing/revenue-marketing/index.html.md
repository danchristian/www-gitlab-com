---
layout: markdown_page
title: "Revenue Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Pages Related to Revenue Marketing

- [Business Operations](/handbook/business-ops)
- [Reseller Handbook](/handbook/resellers/)

## What is Revenue Marketing Handbook

The Revenue Marketing department includes the Sales Development, Field Marketing and Digital Marketing Programs (including Online Marketing & Marketing Program Management). The units in this functional group employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## Revenue Marketing Handbooks

- [XDRs - Inbound/Outbound Sales Development](/handbook/marketing/revenue-marketing/xdr/)
- [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/)
- [Digital Marketing Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/)
- [Marketing Programs Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)
