---
layout: markdown_page
title: "Commercial Sales"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Welcome to the Commercial Sales Handbook

### About 

The Commercial Sales department is part of the overall [GitLab Sales](/handbook/sales) functional group. We focus on [SMB and Mid-Market](/handbook/business-ops/#segmentation) customers to deliver maximum value throughout the entire journey with GitLab.

### Center of Excellence

The Center of Excellence (COE) in the Commercial Sales context refers to the team's research and development of a particular focus area to drive new initiatives and improvements across the organization.  

Why call it Center of Excellence? 
- We have challenges we are uniquely positioned to focus on
- We aren’t the Directly Responsible Individual (DRI) but it is a challenge to get this prioritized
- This is our current platform for collaboration

How?
1. Choose one of the active Topics currently available below. Refer to the team list to see what is unassigned.  Note, to take a topic you do not have to be a Subject Matter Expert (SME) in the area or DRI as they may belong to a different functional group. 
2. Pick an achievable set of commitments and nail it. These should be forecastable and not something too aspirational for the timeframe
3. Decide for yourself if you want that same topic for 3 or 6 months
4. Start with the problem related to your subject that you think needs to be solved

#### Commercial COE Topics
Here are the current active topics for individual team members on COE alignment. This center in its first iteration started with the team's QBR in 2019. 

- **Communication and Collaboration** 
Support on communication and tools to stay better connected as a remote Commercial Sales organization
- **Customer Portal** 
Improvements to our [customer portal](https://customers.gitlab.com)
- **Customer Advisory Board** 
Capturing direct feedback for interaction and organizing customers selected
- **Feedback Loop** 
Improvements to feedback channels to document structured data for all customer interactions
- **Hosted gitlab.com** 
Ramping new customers and users on gitlab.com, our hosted product offering
- **KISS enablement** 
Keep it simple. Focus on process including managing tasks, training GitLab, and new release features
- **Marketing campaigns**
- **Sales content/collateral**
- **Sales process - BDR**
Improvements to the BDR to AE collaboration and working agreement
- **Sales process - SDR**
Improvements to the SDR to AE collaboration process
- **Services**
Improvements to the Professional Services offering
- **Ultimate**
Improvements to the full pitch including [Why Ultimate](https://about.gitlab.com/pricing/ultimate/)

### Give Back Project

#### What is the Give Back Project?

* The Give Back Project is a self-selected opportunity to help the Commercial Sales team achieve better results. It is a finite project that, upon completion, is to be adopted by no less than 70% of the Commercial Sales team and used at least once a month. 

* The Give Back Project is assigned to all new Commercial Sales team members and is required to be completed as part of their on-boarding process. 

#### Why is there a Give Back Project?

* The goal of the Give Back Project is to allow new team members to be able to instantly contribute to the Commercial Sales team 
* Fosters GitLab’s values of collaboration and results
* The Give Back Project gives team members the opportunity to be exposed to problems on the team today and will allow the team to look back and see how we addressed shortcomings in the past

#### How does the Give Back Project differ from the Center of Excellence?

* The COE is assigned while the Give Back is self-selected
* During team QBRs in February 2019, it was determined that there were certain aspects that made selling at GitLab difficult. Those aspects were divided into categories and those categories were assigned to different team members to become experts at, forming the Center of Excellence 
* The COE has already been determined by the team as areas where the team needs improvements while the value of a team member’s Give Back Project needs to be realized
* A Give Back Project can be working on a portion of the COE


#### How to properly document a Give Back Project

* Each Give Back project should live in the Handbook when complete and is to be linked below along with a description of what the Project is
* Although the Give Back Project is designed to be finite, there is always room for iteration at GitLab. Be sure to include any necessary instructions on how a different team member would be able to add to your project if necessary

#### Give Back Projects

**Customer AMAs:** Meetings for GitLab team members with current customers that provide an opportunity to ask them about anything in regards to their experience with GitLab

**Customer Meetings:** Sharing the best practices for conducting any kind of external meeting at GitLab

**Trip Notes:** allows team members who attend field events to track and capture data in order for the rest of the team to gain insight

**Doing Business with Partners/Channels:** Guidelines on how to work with partners and resellers in order to ensure value is maximized in these areas

**How to Handle New Customers After They Purchase:** Best practices for handling new GitLab customers based on the scope of their purchase

**Technical Evaluation of Customers:** How to properly evaluate a customer’s current technical environment in order to create organizational alignment and prescribe how GitLab can best solve their problems


## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Commercial Team Groups
- SMB Customer Advocates
- Mid Market Account Executives
- Area Sales

## SMB Customer Advocates
SMB Advocates act as a primary point of contact and the face of GitLab for [SMB](https://about.gitlab.com/handbook/business-ops/#segmentation) prospects and customers. They are responsible for working new business in their territory as well as handling the customer journey for new and existing customers. SMB Customer Advocates will assist prospects through their evaluation and buying process and will be the customer's point of contact for any renewal and expansion discussions.

### Responsibilities 

https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#flow

#### New Business 
* Work Initial Qualification Meetings [IQM's](https://about.gitlab.com/handbook/sales/commercial/#inbound-queue-management) from BDR Team
* Accept Sales Accepted Opportunities according to [SAO Criteria](https://about.gitlab.com/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao) 
* Nurture and help prospects during their Trial evaluation after Accepting Opportunities
* Push to Webdirect or help with the Sales Order Process 

#### Customer Journey 
* [Inbound Queue Management](https://about.gitlab.com/handbook/sales/commercial/#inbound-queue-management)
* [Licensing/Subscription Mangement](https://about.gitlab.com/handbook/sales/commercial/#licensingsubscription-management)
* [Troubleshooting Resource for Licensing/Subsctiption Mangement](https://about.gitlab.com/handbook/sales/commercial/#troubleshooting-resources-for-licensingsubscription-management)
* [Quotes | Sales Order Processing](https://about.gitlab.com/handbook/sales/commercial/#quotes--sales-order-processing)


### Inbound Queue Management
Zendesk: Managing incoming requests received through support@gitlab.com. 
- Provide response correlated with the account assigned SLA. 
- If the account is within SMB segmentation, respond directly within the ticket if the account belongs to you. If it is owned by an alternate SMB Customer Advocate, cc them within the ticket and submit as open. 
- If the account is outside of SMB segmentation, cc the appropriate account owner within the ticket and submit as open. Ping on Slack to that account manager notifying them of the active ticket which requires their response. 
- If the account is requesting a refund or billing support, disposition the ticket to the Accounts/Receivable Refunds form on the left panel and submit as open. 

Account Management Queue: Managing incoming requests received through renewals@gitlab.com
- Provide a response within 24 hours of time received.
- Assign all cases within the queue to the appropriate account owner by selecting the change icon next to the case owner within the case detail. 
- To those cases assigned to you, respond and disposition the case to closed by selecting close case within the case detail once resolved. 

### Licensing/Subscription Management
License Key Management for On-Premise Trials & Subscriptions
- Access [the LicenseApp](https://license.gitlab.com/) to resend or provide changes to a license key or trial evaluation. 

GitLab.com Trials & Subscription Management 
- Access [the Customer Portal](https://customers.gitlab.com/admin/customer) to provide changes to a user, group, or subscription. 

### Troubleshooting Resources for Licensing/Subscription Management
#### Upgrades

All upgrades are processed manually for both on-premise and GitLab.com customers. To provide an upgrade quote, follow the instructions below:

1. Locate the customers account detail in [Salesforce](https://login.salesforce.com/).
1. Navigate to `Opportunities` located in the account details.
1. Select the earliest opportunity.
1. Create `New Add-On Opportunity`.
1. Once the new add-on opportunity is created, select `New Quote`.
1. Create an [amendment quote](/handbook/business-ops/order-processing/#amend-existing-subscription-for-the-billing-account) adding the additional product.

#### Downgrades

If a customer requests to downgrade the product they are currently using, communicate that we are unable to process refunds within 45 days of purchase; however, we are able to downgrade their plan.

#### True-Up Error 

We [currently charge 100%](/handbook/ceo/pricing/#true-up-pricing) for users added during the year of their subscription. At renewal, it has become a common occurrence that our customers have miscalculated the number of true-up users required for their license. 

The true-up must match the number of users over license data collected on their admin dashboard or else the license will fail to register. 

Example: 

Customer purchased 10 users at the start of their subscription; during the year of their subscription, they had 12 total users in license, adding 2 additional. Their admin dashboard would read 2 users over license. At the time of their renewal, they will be responsible for paying for those 2 users over license in addition to however many users they want to renew for, and once paid for, those 2 users will be added to their new subscription license key. If the true-up is not added or has the incorrect number of users over the license, the license key will not work.

The customer is always free to renew for as many users as they would like, even if this number is lower than the previous year; the true-up amount just needs to be paid for in full. 

If a customer is having issues obtaining the number of licenses needed to purchase, you can request a screenshot from them or else access their usage ping data through https://version.gitlab.com/. If there is no data available through usage ping, we can conclude that the usage ping has not been enabled from the customer, and you will need to obtain this information manually. 

#### Refund Request

For all refund requests, send an email to `AR@gitlab.com` to request a refund for the customer. Include all subscription information and link to SFDC account record. 

### Quotes | Sales Order Processing

More information about [sales order processing](/handbook/business-ops/order-processing/) can be found in the Business Ops handbook section.

## Other Related Pages
- [Commercial Sales - Customer Success](/handbook/customer-success/comm-sales)
- [Business Operations Territories](/handbook/business-ops/#territories)
- [Sales Hiring Chart](/handbook/hiring/charts/sales)
