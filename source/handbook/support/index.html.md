---
layout: markdown_page
title: Support Team Handbook
---

## Welcome to the GitLab Support Team Handbook
{: .no_toc}

The GitLab Support Team provides technical support to GitLab.com and Self-Managed GitLab customers. The GitLab Support Team Handbook is the central repository for why and how we work the way we do.

If you are a customer or advocating for a customer who needs technical assistance, please take a look at the dedicated [Support Page](/support) which describes the best way to get the help you need and lists GitLab's paid service offerings.

If you are a GitLab team-member looking for some help, please see the [Internal Support for GitLab Team Members page](internal-support).

----

### On this page
{:.no_toc}

- TOC
{:toc}

## What does the Support Team do?

### We care for our customers

- Always assume you are the person responsible for ensuring success for the customer.
- When supporting a customer, any issue, incident or loss is _GitLab's loss_.
   - When a customer experiences trouble or downtime, take action with the same urgency you'd have if GitLab were experiencing downtime.
   - When a customer is losing productivity, take action with the same urgency you'd have if GitLab were losing productivity.
   - The rule of thumb is a customer down with 2,500 users gets the same urgency as if GitLab were losing $1,000,000 per day. This treatment is equal regardless of how much they are paying us.
- Escalate early.  Visibility across GitLab, up to and including the CEO, is always better earlier rather than later.  Ensure all resources needed are on the case for customers early.

### How we measure this

We use the below [Key Performance Indicators](/handbook/ceo/kpis/) (KPIs) to keep track of how well the Support team is doing as a whole. 

The KPI measurements can be found under the `Reporting` tab in Zendesk. Progress on meeting these KPIs is also tracked in the [Zendesk Support Dashboard in Periscope](https://app.periscopedata.com/app/gitlab/421422/).

We review these KPIs weekly in the [Support Week-in-Review](/handbook/support/#support-week-in-review). 

#### Service Level Agreement (SLA)
{:.no_toc}

GitLab Support commits to an initial substantive response in a specified amount of time from the time the customer submits a ticket. The SLA for this first reply is based on each customer's [support plan](/support/#gitlab-support-service-levels).

The target is that we meet the Priority Support SLA for the first reply 95% of the time. The definition of the KPI is:

```
Number of Times SLA met / Total Tickets where the SLA was applicable
```

This calculation currently includes only tickets submitted by customers with our top support plans (Premium/Ultimate for Self-Managed and Silver/Gold for GitLab.com) and is usually calculated by calendar month.

#### Customer Satisfaction (CSAT)
{:.no_toc}

CSAT is a measure of how satisfied our customers' are with their interaction with the GitLab Support team. This is based on survey responses from customers after each ticket is solved by the Support team using a Good or Bad rating.

The target is 95% customer satisfaction. The definition of the KPI is:

```
Satisfied [total good scores]  / Total Survey Responses [good scores + bad scores]
```

This calculation excludes cases where a survey was not offered or where it was offered but no score was provided. 

#### Monthly Tickets per Support team member

The Gitlab support team aims to staff the team based on total tickets closed in a given month by an individual support team member involved in customer tickets.  The monthly target per person is 65 tickets and is a function of the tickets closed gearing ratio found below.

```
Total number of monthly tickets  / Total number of support staff
```

#### Customer Support Margin

The Gitlab support team targets a 10% margin as a percentage of ARR for both headcount and non-headcount expenses.

```
Total Customer Support Team Expenses / Monthly Exit ARR
```
### Customer Support Gearing Ratios

Gearing ratios are used as [Business Drivers](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/) to forecast long term financial goals by function.  The two primary gearing ratios for Customer Support are:

* Average Daily Tickets per Support team member
* Manager to IC Ratio

Average Daily Tickets per Support team member is a measure of total daily tickets expected to be completed by an individual support team member.  The long term target for this metric is 3.0.

The Manager to IC Ratio is the ratio of individual contributors to one manager.  The long term target for this metric is 10:1.

Forecasting is an iterative process, in that, we will continue to introduce complexities and variables over time.  Within Customer Support specifically, we look forward to building out a regional gearing process, as well as addressing US Federal and langauge staffing needs.  

## <i class="fas fa-question-circle fa-fw icon-color font-awesome" aria-hidden="true"></i>  Support Direction

The overall direction for Support in 2019 is set by our overall [strategic objectives](/strategy), with a particular emphasis on
- continued improvement of (Premium) Customer satisfaction.
- scaling the team through hiring and process improvements to pace ticket growth
- refining process, communication and team-training to improve efficiency as we scale

### Focal Points of 2019
As can also be inferred from our [publicly visible OKR page](/company/okrs/), we anticipate 2019 will focus on the following elements:

#### Ticket deflection through documentation
{: .no_toc}

1. By building up a corpus of documentation informed by real-world problems we can ensure that customers can get the answers they need before they
come into the queues.
   - *When learning, use the docs. When troubleshooting, use the docs. If something is missing, update the docs.*

1. By developing a [docs-first](/handbook/documentation/#docs-first-methodology) approach to answering, we can ensure that the documentation remains a highly useful [single source of truth](/handbook/documentation/#documentation-as-single-source-of-truth-ssot),
and that customers are more aware of where to find content on their own.
   - *Always respond with a link to the docs. If docs content is missing, create it and link the customer to the MR.*

#### Increase capacity & develop experts
{: .no_toc}

In the past 3 years we expanded the support team significantly, and this trend will continue in 2019. As GitLab -- the product -- continues to expand, so will the skill and knowledge of all Support Engineers to be able to continue providing an excellent customer support experience.

1. Develop "Deep Dives", Bootcamps and other professional training programs to decrease the time it takes for a new team member to be effective.
1. Make sure that professional training sessions result in the production of publicly consumable resources:
docs fixes, tutorials and improvements to any related material.

#### Experiment with support model
{: .no_toc}

In late 2018 we moved from a "product-first" model to a "region-first" model. While this has affected the reporting structure, the lines
of work from the perspective of an individual Support team member continue to be aligned to product. In 2019 we want to experiment with a "concept-first"
model of support that will align individual strengths with customers needs regardless of which queue they come into.

Other areas we'd like to explore:
1. 24x7 capability beyond uptime support (i.e. weekend staffing)
1. Language support
1. US-only Federal Support

### Hazards

#### Team morale suffers because:
{: .no_toc}

| Hazard                     | Commitment                              |
|----------------------------|-----------------------------------------|
| Ticket volume is too high | Build (and adjust) hiring model based on the best data available, hire as quickly as possible while keeping quality up. |
| Team knowledge doesn't match ticket difficulty | Develop training materials, provide time for professional development, encourage specializations.  |
| We aren't hitting our SLOs | Hire and train as per above. Add regularity to scheduling and encourage efficient work patterns.
| Leadership breaks trust    | Communicate in public channels, alert early and often of potential discussions, engender the GitLab value of Transparency. |
| Intra-team trust suffers   | Discussion on-going in [support-team-meta#1447](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1447)|
| Fear of conflict results in poor decisions | Provide focus on meta-issues by triaging issues in the [Active Now](#improving-our-processes---active-now-issue-board) board. Encourage buy-in and bravery. Truly listen and respond. Explicitly overrule: state reasoning and thank everyone for voicing opinions. |
| Team lacks commitment to results or implementing decisions | Ensure voices are heard. Re-enforce "disagree and commit". Build accountability. |
| There's no accountability in poor results or not meeting commitments | Reinforce GitLab value of Results by paying attention and following up. |
| Lack of trust as the team grows | Make an intentional effort to frequently do pairing sessions between regions.|

#### Scaling
{: .no_toc}

As we continue to increase in size, there's a number of challenges that we need to address and/or questions we need to answer:

As we grow:
1. What process inefficiencies do we need to address? (e.g. _Do all engineers need to know about every component/configuration/deployment strategy of GitLab, or are there ways we can specialize?_)
1. Which process inefficiencies do we need to intentionally keep because of a positive externality? (e.g. _Do we keep around a synchronous meeting because it helps build relationships?_)
1. What have we lost in scaling? How can we build it back in in a way that scales? (e.g. _Smaller groups lead to more ideation sessions, how can we make sure we're creating spaces for ideas to build off of each other?_)
1. How can we make sure we're broadcasting things appropriately so no one misses out on an important piece of information?


Know someone who might be a great fit for our team? Please refer them to the job descriptions below.
- [Support Engineer - Self Managed job description](/job-families/engineering/support-engineer).
- [Support Agent - GitLab.com job description](/job-families/engineering/dotcom-support/).

**Increase Documentation Contributions and Usage in Support Cases**

Support engineers should always reply to customer issues with a link, whether to
GitLab's existing documentation or to an MR that the support engineer creates with
new or updated content needed in the docs to address the issue. This [Docs-First methodology](/handbook/documentation/#docs-first-methodology)
is essential for ensuring that the documentation remains a highly useful
[single source of truth](/handbook/documentation/#documentation-as-single-source-of-truth-ssot).
It is not possible or practical to find content across multiple sources including
the Forum, public answers, internal guides, etc., while keeping such disparate sources up to date.

Beyond this work during specific support cases, the team is also asked to contribute their
expertise to GitLab's documentation by producing new guides and videos that would
be useful to our users — especially on emerging DevOps trends and use cases.

Improving the docs enables more users to help themselves, reducing unnecessary support cases.

**Actionable Analytics**

Better reporting on ticket metrics (topic, response times, product it relates to, etc.) will help guide the Support Team's priorities in terms of adding to the greatest documentation needs, bug fixes, and support processes.

### Long Term Profitability Targets

The long term profitability target for support is 90% gross margin.

### Customer Support Gearing Ratio

The headcount staffing methodology is a function of expected monthly tickets based on the Gitlab annual revenue plan.  The relationship between tickets and revenue is based on historical ratios and will continue to be updated as we gather more data.  

The support rep headcount is based off an average daily ticket per support rep assumption of 3.0 derived by taking a weighted average of both self-managed and Gitlab.com monthly tickets.

Manager headcount is a function of support rep headcount and a target ratio of 10:1 manager to individual contributor.

## Team Communications

### Slack

We follow GitLab's [general guidelines for using Slack](https://about.gitlab.com/handbook/communication/#slack) 
for team communications. As only 90 days of activity will be retained, make sure
to move important information into the team handbook, product documentation, 
issue trackers or customer tickets.

#### Channels
{: .no_toc}

| Channel                 | Purpose                                                   |
|-------------------------|-----------------------------------------------------------|
| `#support-team-chat`    | Support team lounge for banter, chat and status updates   |
| `#support_gitlab-com`   | Discuss GitLab.com tickets and customer issues            |
| `#support_self-managed` | Discuss self-managed tickets and customer issues          |
| `#support-managers`     | Discuss matters which require support managers' attention |
| `#spt-hiring`           | Discuss support team hiring-related matters               |

#### User Groups
{: .no_toc}

| Group                  | Who                       |
|------------------------|---------------------------|
| `@support-dotcom`      | GitLab.com Support Team   |
| `@support-selfmanaged` | Self-managed Support Team |
| `@support-team-apac`   | Support Team APAC         |
| `@support-team-emea`   | Support Team EMEA         |
| `@supportmanagers`     | Support Managers          |

If you need to be added to one or more of these groups, please open an issue in 
the [access requests project](https://gitlab.com/gitlab-com/access-requests).

### Weekly Meetings

The Support Team has several meetings each week. These allow us to coordinate and help us all grow together. Each meeting has its own agenda and is led by a different member of the team each week.

|  Weekday  |   Region  |      Meeting Name     |                                        Purpose                                      |
|:---------:|:---------:|:---------------------:|:-----------------------------------------------------------------------------------:|
|  Tuesday  |    APAC   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |
|  Tuesday  |    AMER   |  Casual Catch up |         Agendaless, a space for the team to banter and chat. If you have work to talk about, feel free.        |
| Wednesday | AMER,EMEA | Dotcom Support Call | GitLab.com support team meeting to discuss metrics, demos, upcoming events, and ask questions |
|  Thursday  |    EMEA   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |

The regions listed above are the regions for which each call may be the most convenient, but all are welcome on any call. Every call is recorded and notes are taken on the agenda for each one. If you miss a meeting or otherwise can't make it, you can always get caught up.

All Zoom and agenda links can be found on the relevant calendar entry in the Support Calendar.

Past recordings of our meetings are [available on Google Drive](https://drive.google.com/drive/u/0/folders/0B5OISI5eJZ-DcnBxS05QUTdyekk).

#### Role of the Chair
{: .no_toc}
The main role of the chair is to start the meeting, keep the meeting moving along, and end the meeting when appropriate. There is generally little preparation required, but depending on the meeting, the chair will include choosing a "feature of the week" or similar. Please check the agenda template for parts marked as "filled in by chair."

During the meeting, the chair:

* will ensure that each point in the agenda is covered by the listed person,
* may ask the team to move a discussion to a relevant issue when appropriate,
* copy the agenda template for the following week and tag the next chair/secretary.

If a chair is not available, it is their responsibility to find a substitute.

#### Role of the Notetaker
{: .no_toc}
The notetaker should take notes during the meeting and if action is required, creates a comment and assigns it to the appropriate person.

If the notetaker is not available when it is their turn, they should find a substitute.

### Support Week in Review

Every Friday, we do a week in review, inspired by the greater Engineering organization.  You can add topics any time to the [support week in review google document](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit).

We want to think "issue first" so the majority of the doc should be issues, but also consider
links to cool articles you've found, or awesome pics from recent trips. It should be something like
a 60% work/40% cool stuff blend. We define our culture and this doc is no exception.

On Fridays at 3pm EST, slackbot will remind the team with a link to the week in review and we can use a thread there for banter. Bear in mind that chat older than 90 days will no longer be retained.

You can read about how we got this started
[in this issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1394).

### Support Stable Counterparts
As a result of our direct interactions with customers, the Support Team occupies a unique position in GitLab where
we can connect project managers with the feedback they need to prioritize. 
Each of the [DevOps Stages](/handbook/product/categories/#devops-stages) should have an IC counterpart, and each
section should have a manager counterpart. 

Support Stable Counterparts, briefly, will:
- subscribe to the pertinent trackers / labels to be aware of new issues
- attend product meetings in order to understand the priorities/challenges/upcoming features of the product group
- be aware of the issues raised by customer pertinent to their category, surfacing and advocating for them
- be a subject matter expert in the use of the features related to this group
- be the owner of any special processes or troubleshooting workflows that might pertain to the features in this group

Managers will:
- Fill in for team members who are out
- Facilitate communication between Support and the assigned group
- Help triage and broadcast important issues in weekly communications with the larger support team
- Catalyse training materials and sessions

If you're interested in becoming a stable counterpart for a group, please create an MR on `data/stages.yml` and `source/inclues/product/_categories.erb`
and assign to your manager. 

At writing (Q2-FY20) ICs align to Stage/Product Manager rather than group to keep things easier for PMs. 
Put another way: if the same product manager is in charge of multiple groups in a Stage we will not 
consider assigning an additional stable counterpart.

#### Cross Functional Meetings
Some functions don't fit as cleanly into the Support Stable Counterparts model.  As such we have standing meetings across functions to help surface issues and smooth the interface between such groups.

If you're missing from this list (and want to be on it) please let the Support Managers know in `#support-managers`

If you're on the Support Team and have something you'd like to surface, or would like to attend one of these meetings, feel free to post in `#support-managers`.


| Function   | Party            | Support Manager     | Support Counterpart | Frequency |
|:----------:|:----------------:|:-------------------:|:-------------:|:--------------:|
| Production | Dave Smith       | Lyle Kozloff        |               |every 2 weeks |
| Finance    | Chase Wright     | Tom Cooney          |               |1x Qtr on budget + once per month |
| PeopleOps  | Jessica Mitchell | Tom Cooney          |               |every 2 weeks |
| Recruiting | Cyndi Walsh | Tom Cooney               |               |weekly |
| Docs       | Docs Team        | Tom Atkins          |               |every 2 weeks join docs meeting |
| Sales + CS | EMEA Sales/CS    | Tom Atkins          |               |weekly on Fri join EMEA scrum |



## Team Processes

### Support Workflows

- [All Support Workflows](/handbook/support/workflows)
   - [GitLab.com Support Workflows](/handbook/support/workflows/services)
   - [Support Engineering Workflows](/handbook/support/workflows/support-engineering)
   - [How to Work with Tickets](/handbook/support/workflows/shared/support_workflows/working-with-tickets.html)
   - [How to Submit issues to Product/Development](/handbook/support/workflows/services/support_workflows/issue_escalations.html)
   - [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
   - [How to Submit Docs when working on customer issues](/handbook/documentation) (see 'docs-first methodology')

### Time Off
The Support Team follows [GitLab's paid time off policy](/handbook/paid-time-off). However, do note that if a large number of people are taking the same days off, you may be asked to reschedule. If a holiday is important to you, please schedule it early.

In addition to the tips in [Communicating Your Time Off](/handbook/paid-time-off/#communicating-your-time-off) please also:
- add an event to the [**Support Time-off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) calendar with your name and a brief note, marked "free"
- ensure that you take steps to alert your availability in Calendly (either in the Calendly app, or by making meetings marked "busy" during your working hours)
- if you're on-call, make sure you have coverage and that PagerDuty is up to date
- reschedule any 1:1 meetings with your manager
- add an agenda item during the team meeting before your time off
- add an agenda item after you get back to let us know what you've been up to!

If you do not have access to the [**Support Time-off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) team calendar, please raise it in the `#support-team-chat` channel on Slack and someone will share it with you.

You do **not** need to add time-off events to the shared calendar if you'll be gone less than half a day. *Do* consider blocking off your
own calendar so that customer calls or other meetings won't be scheduled during your time away.

If you need to go for a run, grab a coffee or take a brain break please do so without hesitation.

### Onboarding

- [Support Onboarding](/handbook/support/onboarding)
   - [Support Engineer Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20Self-managed%20Support%20Engineer.md)
   - [GitLab.com Support Agent Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Agent.md)

### What if I Feel Threatened or Harassed While Handling a Support Request?
Just as Support Team are expected to adhere to the Code of Conduct, we also expect customers to treat the Support Team
with the same level of respect.

If you receive threatening or hostile emails from a user, please create a confidential issue in the GitLab Support Issue Tracker
and include:

1. A history of the user's communication
1. Relevant links
1. Summary of the high-level issues
1. Any other supporting information

Include your Manager, Director of Support, Abuse Team and Chief People Officer in this issue. Together we will evaluate on a
case-by-case basis whether to take action (e.g. ban the user from the forums for violating the [Code of Conduct](/handbook/people-operations/code-of-conduct)).

### Improving our processes - 'Active Now' issue board
The Support team use ['support-team-meta' project issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) to track ideas and initiatives to improve our processes. The ['Active Now' issue board](https://gitlab.com/gitlab-com/support/support-team-meta/boards/580661) shows what we're currently working on. It uses three labels:

1. **Blocked** - waiting for another team or external resource before we can move ahead
1. **Discussing this week** - under active discussion to arrive at a decision
1. **In Progress** - actively being worked on

Some principles guide how these labels are used:

1. A **maximum of six issues** at any time for each label (18 total issues)
1. All issues with one of the above labels must be **assigned** to one or more support team members
1. All issues with one of the above labels must have a **due date** no longer than a week ahead
1. If an issue is too big to complete in a week it should be **split into smaller parts that can be completed in a week** (a larger 'parent' issue is OK to keep in the project, but it shouldn't make it onto the 'In Progress' column)

**Each week we look at the board and discuss the issues to keep things moving forward.**

By keeping a maximum of six issues for each label, we **limit work in progress** and make sure things are completed before starting new tasks.

**Adding and managing items on the board:**

Support managers will regularly review the board to keep items moving forward.

1. The team can **vote on issues not on the board** by giving a 'thumbs up' emoji so we can see [popular issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues?sort=popularity&state=opened).
1. Support managers will look at popular issues and add them to the board when there is room.
1. Support managers will **curate** issues to prevent a large backlog. Unpopular or old issues can be closed / merged to keep the backlog manageable.

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Support Resources

- [Support Hiring &amp; Managers](/handbook/support/managers)
- [Support Channels](/handbook/support/channels)
- [On-Call](/handbook/on-call/)
- [Support Ops](/handbook/support/support-ops)
- [Advanced Topics](/handbook/support/advanced-topics)
