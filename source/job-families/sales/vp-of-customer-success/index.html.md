---
layout: job_family_page
title: "Vice President of Customer Success"
---

At GitLab, we are fundamentally changing the way our customers get their software to market by putting the entire DevOps lifecycle into a single application.  With over 100,000 organizations using the product GitLab is one of the fastest growing companies in technology.  Our customer success teams are responsible for ensuring that our customers are wildly successful in achieving their business outcomes with the GitLab product as they move to truly modern DevOps.  A complete long-term engagement of planning, strategy, coaching, services and relationship building ensures our customers exceed their goals and digitally transform. We know that our customers trust GitLab to take their ideas all the way from plan to shipped product in production and we don’t take that responsibility lightly. We have an incredible existing, and rapidly growing, customer base, with a passionate, supportive open-source community and incredibly talented teams located in 40 countries focused on supporting them.

We're looking for an experienced Customer Success and Professional Services leader to lead our Customer Success, Professional Services, and Solutions Architect teams.

As Vice President of Customer Success, you will be responsible for bringing your strategic vision and innovative approach to lead critical customer-facing teams at GitLab.  Reporting to the Chief Revenue Officer, you will play a key role in driving customer goals, product utilization, business transformation, and revenue expansion by ensuring the engagement, success, retention, and growth of GitLab’s customers.

If you have an action mindset and are excited by the idea of quickly scaling globally, this is a rare opportunity to join a bold, fast-moving, transparent, values-driven leadership team and company while enabling both customer and company success.

## Responsibilities

* Leading, expanding, and mentoring the Customer Success teams by setting the strategy and prioritizing Objectives and Key Results (OKRs); hiring, training, and developing a world-class team
* Driving customer lifetime value by defining the customer journey; deploying programs to help drive business value with customers, customer goal achievement, new features, and new use-cases; collaborating across teams to identify and pursue customer growth opportunities
* Representing the voice of the customer and influencing internal stakeholders by promoting a customer-centric mindset across the organization
* Architecting the customer success organization and solutions to leverage and scale in support of our revenue ambitions, including striking the right balance for the services and support offered to our broad range of customer segments - Large Enterprise, Mid-market, SMB, and Free/Open
* Build a global professional services organization to support customers from onboarding and training through to long-term multi-stage digital transformation project.
* Partner with 3rd party systems integrators for services scale and project delivery as part of major cloud-migration and digital transformation projects.
* Partnering very closely with our sales teams to engage with leaders at prospective customers and existing customers to define goals and leverage our products and services to achieve them.


## Requirements

* A true love for customers
* 10+ years of experience in a Customer Success and/or Professional Services leadership role. Sales experience will also be considered
* Experience building and managing large (100+ employees) Customer Success and or Professional Services teams in a fast-paced, dynamic environment
* Ability to move quickly and iterate
* Technical and SaaS experience and an ability to speak to technical customers in their language
* A strong strategic vision for the customer experience, professional services, and customer support
* The ability to architect services and support delivery models that align with current customer segments, deliver customer value and scale with growth projections
* A strong customer advocate with the ability and willingness to engage directly with customers
* Ability to communicate well with individuals, teams, partners and at industry level events
* A track record of developing and mentoring great talent, and building and motivating high achieving teams
* The skills to be a data-driven decision maker, with a willingness to experiment and iterate
* Understanding of the balance between internal services, external partners and how to use both effectively to support customers and grow quickly
* Effective and productive collaborator to drive cross-functional initiatives
* Empathy, humility and listening skills
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#s-group)
