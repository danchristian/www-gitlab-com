---
layout: job_family_page
title: "Area Sales Manager"
---

We are looking for Area Sales Managers (ASM) in all of our territory selling markets. As the ASM, you will lead a dynamic sales team and help GitLab surpass our growth goals. Reporting to the Regional Sales Director, you will be responsible for hiring, developing and leading a high-performing team of Strategic Account Leaders to meet and exceed sales goals. The right candidate is truly passionate about open source software and has a proven track record to talk about! This is an exciting opportunity to unequivocally influence GitLab’s overall success and growth.

## Responsibilities

- Drives, manages and executes the business and revenue of a sales team
- Analyzes market dynamics in an effort to maximize existing successes and to create new sales growth opportunities
- Prepares forecasts, territory/industry management, and growth plans
- Educates team on significant industry factors including competitive products, regulations, trends, customer needs, and pricing
- Establishes and reports on metrics to measure team performance; correct deficiencies where necessary
- Ensures that the sales plan is aligned with and supports the corporate revenue goal
- Manages a team of 5-10 Strategic Account Leaders; fosters a successful and positive team environment

## Requirements

- 10+ years experience in field sales, operations and leadership in open source software or software DevOps environment
- Experience selling to Fortune 500; willingness to “roll up your sleeves” and sell
- Proven track record of meeting or exceeding performance objectives (revenue targets, pipeline targets, etc.)
- Experience utilizing CRM systems and marketing automation systems (such as Salesforce, Clari, Marketo, etc).
- Ability to exercise effective judgment, sensitivity, creativity to changing needs and situations; ability to handle a fast-paced environment and challenging workload
- Strong relationship building and negotiation skills
- Strong presentation skills; Executive level communication skills (both written and verbal) and the ability to mentor others
- Ability to travel as needed
- You share our values, and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Specialties

### Federal

#### Additional Requirements

In addition to the areas outlined above, Federal ASM’s have the following additional responsibilities and requirements:
- Work closely with Director of Public Sector Sales to develop and execute Strategic Account Plans for key Federal agencies.
- At least five 5 years of sales management, selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
- At least 10 years of experience selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
- Must be a high performing sales manager with a proven track record of consistently exceeding established measurements for goals and objectives into Fed
- Must have expertise around GSA, Federal procurement and the various buying vehicles to enable growth in software sales via our distribution partners
- Knowledge of open source enterprise software and/or version control is highly desirable
- Exceptional knowledge of Federal infrastructure and agencies
- A security clearance is a plus
